using UnityEngine;

class AddReward : GameAction
{

    public enum RewardTypes
    {
        Friendos,
        Boops,
        Schmackos
    }

    public RewardTypes RewardType;
    public int amount = 1;
    
    public override void ExecuteAction()
    {
        switch (RewardType)
        {
            case RewardTypes.Friendos:
                Tm.ChangeFriendosCount(amount);
                break;
            case RewardTypes.Boops:
                Tm.ChangeBoopsCount(amount);
                break;
            case RewardTypes.Schmackos:
                Tm.ChangeSchmackosCount(amount);
                break;
            default:
                Debug.LogError("Reward type not set");
                break;
        }
        FinishAction();
    }
}