﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TextChangeAction : GameAction
{
    
    public string TextChange;

    private Collider2D _collider;
    private float _timeElapsed;
    
    public override void ExecuteAction()
    {
        _isActive = true;
        ChangeText(TextChange);
        if (!IsTimed)
        {
            FinishAction();
        }
    }

    private void Update()
    {
        if (!_isActive) return;
        if (!IsTimed) return;
        
        _timeElapsed += Time.deltaTime;
        
        if (_timeElapsed >= timer)
        {
            FinishAction();
        }
    }
}
