using UnityEngine;

class QuitGame : GameAction
{
    public override void ExecuteAction()
    {
        Application.Quit();
    }
}