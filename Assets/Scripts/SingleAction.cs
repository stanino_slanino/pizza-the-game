﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static ControlScheme;

public class SingleAction : GameAction
{
    public string String;
    
    
    public override void ExecuteAction()
    {
        ChangeText(String);
        _isActive = true;
    }

    private void LateUpdate()
    {
        if (!_isActive) return;
        if (Input.GetKeyDown(KeyPrimary))
        {
            FinishAction();
        }
    }
}
