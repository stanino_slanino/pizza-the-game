using UnityEngine;

class StartAnimation : GameAction
{
    public string AnimationName;
    private float _timeElapsed;
    public override void ExecuteAction()
    {
        Animator a = GetComponentInParent<Animator>();
        a.SetTrigger(AnimationName);
        _isActive = true;
    }

    private void Update()
    {
        if (!_isActive) return;
        if (IsTimed)
        {
            _timeElapsed += Time.deltaTime;
            if (!(_timeElapsed >= timer)) return;
            FinishAction();
            _isActive = false;
            _timeElapsed = 0;
        }
    }
}