﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static ControlScheme;

public class ChoiceAction : GameAction
{

    public string Text;
    public GameAction PrimaryAction;
    public GameAction SecondaryAction;

    private void LateUpdate()
    {
        if (!_isActive) return;
        if (Input.GetKeyDown(KeyPrimary))
        {
            PrimaryAction.Et = Et;
            PrimaryAction.ExecuteAction();
            _isActive = false;
        }
        else if (Input.GetKeyDown(KeySecondary))
        {
            PrimaryAction.Et = Et;
            SecondaryAction.ExecuteAction();
            _isActive = false;
        }
        
    }

    public override void ExecuteAction()
    {
        _isActive = true;
        ChangeText(Text);
    }
}
