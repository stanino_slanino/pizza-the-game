﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static ControlScheme;

public class PizzaController : MonoBehaviour
{

   private Animator _animator;
   public AnimationController Pizza;
   public float MoveSpeed = 5;
   private bool _isWalking;
   private bool _canWalk = true;
   
   private void OnEnable()
   {
      _animator = GetComponentInChildren<Animator>();
   }

   private void Update()
   {
      _isWalking = ResolveInput();
      _animator.SetBool("isWalking", _isWalking);
   }

   public void SetWalkingState(bool state)
   {
      _canWalk = state;
   }

   public void SetEarsBack(bool ears)
   {
      _animator.SetBool("isConsterned", ears);
   }

   private bool ResolveInput()
   {
      if (!_canWalk) return false;
      if (!Input.GetKey(KeyMove)) return false;
      _isWalking = true;
      transform.position += Vector3.right*Time.deltaTime*MoveSpeed;
      return true;
   }
}
