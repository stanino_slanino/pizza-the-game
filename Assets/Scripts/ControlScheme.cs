﻿using UnityEngine;

public static class ControlScheme
{
    public const KeyCode KeyMove = KeyCode.W;
    public const KeyCode KeyPrimary = KeyCode.E;
    public const KeyCode KeySecondary = KeyCode.Q;
}
