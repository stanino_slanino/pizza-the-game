﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using UnityEngine;

public class EventTrigger : MonoBehaviour
{

    public List<GameAction> Actions;
    public float DissolveTimeout = 0.5f;

    private PizzaController _pizza;
    private bool _timeoutStarted = false;
    private float _timeoutElapsed = 0f;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!other.CompareTag("Player")) return;
        _pizza = FindObjectOfType<PizzaController>();
        _pizza.SetWalkingState(false);
        _pizza.SetEarsBack(true);
        List<GameAction> childActions = GetComponentsInChildren<GameAction>().ToList();
        foreach (GameAction childAction in childActions)
        {
            childAction.Et = this;
        }
        TriggerNextAction();
    }

    private void OnTriggerExit(Collider other)
    {
        
    }

    public void TriggerNextAction()
    {
        if (Actions.Count == 0)
        {
            CleanupActions();
            return;
        }
        GameAction action = Actions[0];
        Actions.RemoveAt(0);
        action.ExecuteAction();
    }

    private void CleanupActions()
    {
        _pizza.SetWalkingState(true);
        _pizza.SetEarsBack(false);
        _timeoutStarted = true;
    }

    public void AddActions(List<GameAction> actions)
    {
        foreach (GameAction action in actions)
        {
            Actions.Insert(0, action);
        }
    }

    private void Update()
    {
        if (!_timeoutStarted) return;
        _timeoutElapsed += Time.deltaTime;
        if (_timeoutElapsed >= DissolveTimeout)
        {
            gameObject.SetActive(false);
        }
    }
}
