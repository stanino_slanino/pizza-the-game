using UnityEngine;

class SwitchGraphicsAction : GameAction
{
    public GameObject DeactivatedGameObject;
    public GameObject ActivatedGameObject;
    
    public override void ExecuteAction()
    {
        DeactivatedGameObject.SetActive(false);
        ActivatedGameObject.SetActive(true);
        FinishAction();
    }
}