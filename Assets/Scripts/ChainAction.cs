using System.Collections.Generic;
using UnityEngine;

class ChainAction : GameAction
{
    public List<GameAction> Actions;
    private float _startTimer = 0.1f;
    private float _elapsedTimer = 0.1f;
    public override void ExecuteAction()
    {
        Et.AddActions(Actions);
        _isActive = true;
    }

    private void Update()
    {
        if (!_isActive) return;
        if (_elapsedTimer >= _startTimer)
        {
            FinishAction();
            return;
        }
        _elapsedTimer += Time.deltaTime;
    }
}