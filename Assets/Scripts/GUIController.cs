﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GUIController : MonoBehaviour
{
    public TextMeshProUGUI InfoText;
    public TextMeshProUGUI FriendosCountText;
    public TextMeshProUGUI BoopsCountText;
    public TextMeshProUGUI SchmackosCountText;
    
    public int FriendosCount;
    public int BoopsCount;
    public int SchmackosCount;

    public void ChangeFriendosCount(int change)
    {
        FriendosCount += change;
        FriendosCountText.text = FriendosCount.ToString();
    }
    
    public void ChangeBoopsCount( int change)
    {
        BoopsCount += change;
        BoopsCountText.text = BoopsCount.ToString();
    }
    
    public void ChangeSchmackosCount(int change)
    {
        SchmackosCount += change;
        SchmackosCountText.text = SchmackosCount.ToString();
    }
}
