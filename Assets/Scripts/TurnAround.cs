using UnityEngine;

class TurnAround : GameAction
{
    public GameObject Actor;
    public override void ExecuteAction()
    {
        Vector3 localScale = Actor.transform.localScale;
        localScale = new Vector3(-localScale.x, localScale.y, localScale.z);
        Actor.transform.localScale = localScale;
        FinishAction();
    }
}