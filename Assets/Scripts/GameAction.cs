﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;
using static ControlScheme;

public abstract class GameAction : MonoBehaviour
{
    protected GUIController Tm;
    protected bool IsTimed = false;
    public float timer;
    protected bool _isActive = false;

    private EventTrigger _et;

    public EventTrigger Et
    {
        get { return _et; }
        set { _et = value; }
    }

    public abstract void ExecuteAction();

    protected void FinishAction()
    {
        _et.TriggerNextAction();
        _isActive = false;
    }

    protected void ChangeText(string text)
    {
        Tm.InfoText.text = text;
    }
    
    private void OnEnable()
    {
        Tm = FindObjectOfType<GUIController>();
        if (timer != 0)
        {
            IsTimed = true;
        }
    }
}