﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationController : MonoBehaviour
{
    public void ReverseDirection()
    {
        Vector3 newScale = new Vector3(transform.localScale.x*-1, transform.localScale.y, transform.localScale.z);
        transform.localScale = newScale;
    }
}
